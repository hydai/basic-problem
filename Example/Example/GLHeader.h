#ifndef INC_ENGHEADER_H
#define INC_ENGHEADER_H
#include <GLUT/glut.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>

typedef uint8_t BYTE;
void InitialGame(int x,int y,const char *title);
void DrawText(const char *string,float x,float y,float R,float G,float B);
void DrawLine(float x1,float y1,float x2,float y2,float R,float G,float B);
void DrawPolygon(float *x,float *y,int n,float R,float G,float B);
void MainLoop();
void Update();
void SetLevel(int Level);
int GetLevel();
void LevelTest();
void ClearScreen();
//-----IMAGE-----

GLuint LoadBMP256(const char *fn);
GLuint LoadBMPToTrans(const char *fn);
void DrawImage(GLuint ImageIndex,float x,float y,float width,float height);

#endif

