#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int i, a1, a2, a3, b1, b2, b3;
    for (i = 1; i <= 10; i++) {
        FILE *fin, *fout;
        char str[10];
        sprintf(str, "%d.in", i);
        fin = fopen(str, "w");
        sprintf(str, "%d.out", i);
        fout = fopen(str, "w");
        fprintf(fin, "%d\n", i);
        fclose(fin);
        fclose(fout);
    }

    return 0;
}
