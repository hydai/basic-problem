#include"GLheader.h"
#include<GLUT/glut.h>
#include<stdlib.h>
#include<stdio.h>
#include<assert.h>
#define __BMPSHIFT 54
int __Level = 1;
void InitialGame(int x,int y,const char *title)
{
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(x,y);
    glutInitWindowPosition(80,80);
    glutCreateWindow(title);
    __Level = 1;
    return;
}
void DrawText(const char *string,float x,float y,float R,float G,float B)
{
    glColor3f((GLfloat)R,(GLfloat)G,(GLfloat)B);
    x = x - 1 , y = 1 - y;
    const char *c;
    glRasterPos2f((GLfloat)x,(GLfloat)y);
    for(c = string; *c != '\0'; c++)
        glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, *c);
    glColor3f(1,1,1);
    return;
}
void DrawLine(float x1,float y1,float x2,float y2,float R,float G,float B)
{
    glColor3f((GLfloat)R,(GLfloat)G,(GLfloat)B);
    x1 = x1 - 1 , y1 = 1 - y1;
    x2 = x2 - 1 , y2 = 1 - y2;
    glBegin(GL_LINES);
        glVertex3f((GLfloat)x1,(GLfloat)y1,((GLfloat)__Level)*0.01);
        glVertex3f((GLfloat)x2,(GLfloat)y2,((GLfloat)__Level)*0.01);
    glEnd();
    glColor3f(1,1,1);
    return;
}
void DrawPolygon(float *x,float *y,int n,float R,float G,float B)
{
    glBegin(GL_POLYGON);
        for(int lx = 0;lx < n;lx++)
        {
            float xx = x[lx] - 1;
            float yy = 1 - y[lx];
            glColor3f((GLfloat)R,(GLfloat)G,(GLfloat)B); 
            glVertex3f((GLfloat)xx,(GLfloat)yy,((GLfloat)(__Level))*0.01);
        }
    glEnd();
    glColor3f(1,1,1);
    return;
}
void SetLevel(int Level)
{
    if((Level < 1) or (Level > 100))
    {
        printf("SetLevel : Set level fail!\n");
        return;
    }
    //assert((Level >= 1));
    //assert((Level <=100));
    __Level = Level;return;
}
int GetLevel(){return __Level;}
void LevelTest(){glEnable(GL_DEPTH_TEST);return;}
void MainLoop(){glutMainLoop();return;}
void Update(){glutSwapBuffers();return;}
void ClearScreen()
{
    glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    return;
}

GLuint LoadBMP256(const char *fn)
{
    GLuint texture;
    int width=256, height=256;

    FILE *file;
    // open texture data
    file = fopen(fn, "rb" );
    if (file == NULL){ 
        printf("LoadBMP256 : Fail to load bmp file!\n");
        return 0;
    }

    fseek(file, 18, SEEK_SET);
    fread(&width, sizeof(int), 1, file);
    fread(&height, sizeof(int), 1, file);

    printf("Load img, width = %d, height = %d\n", width, height);
    BYTE *data=(BYTE*)malloc(width*height*3 + __BMPSHIFT);
    BYTE *datt=(BYTE*)malloc(width*height*3);

    // allocate buffer
    fseek(file, 0,SEEK_SET);
    fread(data,width*height*3 + __BMPSHIFT, 1, file);
    fclose(file);

    // allocate a texture name
    glGenTextures( 1, &texture );


    // select our current texture
    glBindTexture( GL_TEXTURE_2D, texture );

    // select modulate to mix texture with color for shading
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    // when texture area is small, bilinear filter the closest MIP map
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                   GL_LINEAR_MIPMAP_NEAREST );

    // when texture area is large, bilinear filter the first MIP map
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // if wrap is true, the texture wraps over at the edges (repeat)
    //       ... false, the texture ends at the edges (clamp)
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // build our texture MIP maps

    //BMP
    for(int lx = __BMPSHIFT;lx < width*height*3 + __BMPSHIFT;lx += 3)
    {
        BYTE tmp;
        tmp = data[lx];
        data[lx] = data[lx+2];
        data[lx+2] = tmp;
    }


    for(int lx = 0;lx < height;lx++)
        for(int ly = lx*width*3;ly < (lx + 1)*width*3;ly++)
            datt[(height - 1 - lx)*width*3 + (ly - lx*width*3)] = data[ly + __BMPSHIFT];

    gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width,height, GL_RGB , GL_UNSIGNED_BYTE, datt);

    // free buffer
    free(data);
    free(datt);
    return texture;
}
GLuint LoadBMPToTrans(const char *fn)
{
    GLuint texture;
    int width=256, height=256;

    FILE *file;
    // open texture data
    file = fopen(fn, "rb" );
    if (file == NULL){
        printf("LoadBMPToTrans : Fail to load bmp file QAQ\n");
        return 0;
    }

    fseek(file, 18, SEEK_SET);
    fread(&width, sizeof(int), 1, file);
    fread(&height, sizeof(int), 1, file);
    printf("Load img, width = %d, height = %d\n", width, height);

    BYTE *data = (BYTE*)malloc(width*height*3 + __BMPSHIFT);
    BYTE *datt = (BYTE*)malloc(width*height*3);
    BYTE *TRS  = (BYTE*)malloc(width*height*4);//=(BYTE*)malloc(width*height*4);
     



    // open texture data
    file = fopen( fn, "rb" );
    if ( file == NULL ) return 0;

    // allocate buffer

    // read texture data
    fseek(file, 0,SEEK_SET);
    fread( data, width*height*3 + __BMPSHIFT, 1, file );
    fclose( file );

    // allocate a texture name
    glGenTextures( 1, &texture );


    // select our current texture
    glBindTexture( GL_TEXTURE_2D, texture );
    // select modulate to mix texture with color for shading
    //glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    //
    glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
    glTexImage2D( GL_TEXTURE_2D,0, 4, width, height,0, GL_RGBA, GL_UNSIGNED_BYTE, TRS );

      // when texture area is small, bilinear filter the closest MIP map
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                   GL_LINEAR_MIPMAP_NEAREST );

    // when texture area is large, bilinear filter the first MIP map
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // if wrap is true, the texture wraps over at the edges (repeat)
    //       ... false, the texture ends at the edges (clamp)
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // build our texture MIP maps
    //BMP
    BYTE tmp;
    for(int lx = __BMPSHIFT;lx < width*height*3 + __BMPSHIFT;lx+=3)
    {
        tmp = data[lx];
        data[lx] = data[lx+2];
        data[lx+2] = tmp;
    }


    for(int lx = 0;lx < height;lx++)
        for(int ly = 0;ly < width*3;ly++)
            datt[(height-1-lx)*width*3 + ly] = data[lx*width*3+ly + __BMPSHIFT];

    for(int i = 0;i < height;i++)
    {
        for(int j = 0;j < width;j++)
        {
            *(TRS+i*width*4+j*4 + 0) = *(datt+i*width*3+j*3 + 0);
            *(TRS+i*width*4+j*4 + 1) = *(datt+i*width*3+j*3 + 1);
            *(TRS+i*width*4+j*4 + 2) = *(datt+i*width*3+j*3 + 2);
            if( ( *(TRS+i*width*4+j*4 + 0) == 255) and
                ( *(TRS+i*width*4+j*4 + 1) == 255) and
                ( *(TRS+i*width*4+j*4 + 2) == 255))
                *(TRS+i*width*4+j*4 + 3) = 0;
            else
                *(TRS+i*width*4+j*4 + 3) = 256;
        }
    }
    gluBuild2DMipmaps( GL_TEXTURE_2D, 4, width, height, GL_RGBA , GL_UNSIGNED_BYTE, TRS );
    // free buffer

    free(data);
    free(datt);
    free(TRS);
    //glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
    return texture;
}
void DrawImage(GLuint ImageIndex,float x,float y,float width,float height)
{
    float xs[] = {x, x + width, x + width, x};
    float xc[] = {0, 1, 1, 0};
    float ys[] = {y, y, y + height, y + height};
    float yc[] = {0, 0, 1, 1};
    
    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D,ImageIndex);
    glBegin(GL_POLYGON);
        for(int lx = 3;lx >= 0;lx--)
        {
            xs[lx] = xs[lx] - 1 , ys[lx] = 1 - ys[lx];
            glTexCoord2d((GLfloat)(xc[lx]),(GLfloat)(yc[lx]));
            glVertex3f((GLfloat)(xs[lx]),(GLfloat)(ys[lx]),((GLfloat)(__Level))*0.01);
        }
    glEnd();
    glDisable(GL_TEXTURE_2D);
    return;
}
