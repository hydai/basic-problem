#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char *argv[])
{
    int ct, i;
    scanf("%d", &ct);
        FILE *infile = fopen("in", "r");
        FILE *outfile = fopen("out", "r");
    for (i = 1; i <= ct; i++) {
        char finstr[10], foutstr[10];
        sprintf(finstr, "%d.in", i);
        sprintf(foutstr, "%d.out", i);

        FILE *fin = fopen(finstr, "w");
        FILE *fout = fopen(foutstr, "w");
        int a, b, c;

        fscanf(infile, "%d%d", &a, &b);
        fprintf(fin, "%d %d\n", a, b);
        fscanf(outfile, "%d", &c);
        fprintf(fout, "%d\n", c);
        fclose(fin);
        fclose(fout);
    }
        fclose(infile);
        fclose(outfile);
    return 0;
}
