#include <stdio.h>
#include <stdlib.h>
#define DATASIZE 100
int main(int argc, char *argv[])
{
    FILE *fin, *fout;
    fin = fopen("1.in", "w");
    fout = fopen("1.out", "w");

    int a, b, i;
    for (i = 0; i < DATASIZE; i++) {
        a = rand()%DATASIZE;
        b = rand()%DATASIZE;
        fprintf(fin, "%d %d\n", a, b);
        fprintf(fout, "%d %d\n", b, a);
    }

    fclose(fin);
    fclose(fout);
    return 0;
}
