#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int a, b, c;
    FILE *fin, *fout;
    fin = fopen("1.in", "w");
    fout = fopen("1.out", "w");
    while (~scanf("%d%d%d", &a, &b, &c)) {
        fprintf(fin, "%d %d\n", a, b);
        fprintf(fout, "%d\n", c);
    }
    fclose(fin);
    fclose(fout);
    return 0;
}
