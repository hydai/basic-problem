#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *in, *out;
    int a, b, c;
    in = fopen("1.in", "r");
    out = fopen("1.out", "r");
    while (~fscanf(in, "%d%d", &a, &b)) {
        fscanf(out, "%d", &c);
        if (a*a + b*b != c*c) {
            printf("%d %d %d ERROR\n", a, b, c);
        }
    }
    fclose(in);
    fclose(out);
    return 0;
}
