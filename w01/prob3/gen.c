#include <stdio.h>
#include <stdlib.h>
#define DATASIZE 1000
int main(int argc, char *argv[])
{
    FILE *fin, *fout;
    fin = fopen("1.in", "w");
    fout = fopen("1.out", "w");

    int MaxCD, CurCD, Times, i;
    for (i = 0; i < DATASIZE/10; i++) {
        MaxCD = rand()%DATASIZE;
        Times = rand()%DATASIZE;
        CurCD = rand()%DATASIZE;
        if (CurCD > MaxCD) {
            CurCD = MaxCD;
        }
        fprintf(fin, "%d %d %d\n", MaxCD, CurCD, Times);
        CurCD += Times/10;
        fprintf(fout, "%d\n", (MaxCD < CurCD)?MaxCD:CurCD);
    }

    fclose(fin);
    fclose(fout);
    return 0;
}
