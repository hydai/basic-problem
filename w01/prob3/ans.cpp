#include <cstdio>
int main() {
    int MaxCD, CurCD, Times, FinalCD;
    scanf("%d%d%d", &MaxCD, &CurCD, &Times);
    FinalCD = CurCD + Times/10;
    FinalCD = (FinalCD > MaxCD)?MaxCD:FinalCD;
    printf("%d\n", FinalCD);
    return 0;
}
