#include <stdio.h>
#include <stdlib.h>
#define DATASIZE 1000
int main(int argc, char *argv[])
{
    FILE *fin, *fout;
    fin = fopen("1.in", "w");
    fout = fopen("1.out", "w");

    int a, b, i;
    for (i = 0; i < DATASIZE/10; i++) {
        a = rand()%DATASIZE;
        b = (a/6)*6;
        fprintf(fin, "%d\n", a);
        fprintf(fout, "%d\n", b);
    }

    fclose(fin);
    fclose(fout);
    return 0;
}
