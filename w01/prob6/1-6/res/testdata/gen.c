#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
    int i, a1, a2, a3, b1, b2, b3;
    for (i = 1; i <= 10; i++) {
        a1 = rand()%10000;
        a2 = rand()%10000;
        a3 = rand()%10000;
        b1 = rand()%10000;
        b2 = rand()%10000;
        b3 = rand()%10000;
        FILE *fin, *fout;
        char str[10];
        sprintf(str, "%d.in", i);
        fin = fopen(str, "w");
        sprintf(str, "%d.out", i);
        fout = fopen(str, "w");
        fprintf(fin, "%d %d %d %d %d %d\n", a1, a2, a3, b1, b2, b3);
        fprintf(fout, "%d (%d,%d,%d)\n", a1*b1+a2*b2+a3*b3, a2*b3-a3*b2, a3*b1-a1*b3, a1*b2-a2*b1);
        fclose(fin);
        fclose(fout);
    }

    return 0;
}
