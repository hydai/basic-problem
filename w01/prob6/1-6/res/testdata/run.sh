#!/bin/bash
read -p "Input the number of testcase: " nu

for (( i=1; i<=$nu; i=i+1 ))
do
    ./a.out < ${i}.in > output
    diff output ${i}.out
    rm output
done
