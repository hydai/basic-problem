#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int a, b, c, i;
    FILE *fin, *fout;
    fin = fopen("1.in", "w");
    fout = fopen("1.out", "w");
    for (i = 1; i <= 1000; i++) {
        c = rand()%1000+1;
        if (i % 2 == 0) {
            fprintf(fin, "%d %d\n", c*i, c);
            fprintf(fout, "%d\n", i);
        }
    }
    fclose(fin);
    fclose(fout);
    return 0;
}
