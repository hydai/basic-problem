#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int mv[8][2] = {
    {-1, -1},
    {-1, 0},
    {-1, 1},
    {0, -1},
    {0, 1},
    {1, -1},
    {1, 0},
    {1, 1}
};
int arr[1050][1050] = {0};
int ans[1050][1050] = {0};

int main(int argc, char *argv[])
{
    int n, m, i, j, k;
    FILE *fin, *fout;
    fin = fopen("in", "w");
    fout = fopen("out", "w");
    scanf("%d%d", &n, &m);
    srand(time(NULL));
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= m; j++) {
            arr[i][j] = rand()%2;
        }
    }
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= m; j++) {
            for (k = 0; k < 8; k++) {
                ans[i][j] += arr[i + mv[k][0]][j + mv[k][1]];
            }
        }
    }
    fprintf(fin, "%d %d\n", n, m);
    for (i = 1; i <= n; i++) {
        fprintf(fin, "%d", arr[i][1]);
        for (j = 2; j <= m; j++) {
            fprintf(fin, " %d", arr[i][j]);
        }
        fprintf(fin, "\n");
    }
    for (i = 1; i <= n; i++) {
        fprintf(fout, "%d", ans[i][1]);
        for (j = 2; j <= m; j++) {
            fprintf(fout, " %d", ans[i][j]);
        }
        fprintf(fout, "\n");
    }
    return 0;
}

