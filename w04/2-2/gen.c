#include <stdio.h>
#include <stdlib.h>
#include <time.h>
char map[128] = {0};
int map_ct = 0;
int main(int argc, char *argv[])
{
    int i;
    srand(time(NULL));
    for (i = 'A'; i <= 'Z'; i++) {
        map[map_ct++] = i;
    }
    for (i = 'a'; i <= 'z'; i++) {
        map[map_ct++] = i;
    }
    map[map_ct++] = ' ';
    map[map_ct++] = '!';
    map[map_ct++] = '#';
    map[map_ct++] = '@';
    map[map_ct++] = '.';
    map[map_ct++] = ',';
    for (i = 0; i < 5; i++) {
        int ct = 990;
        while (ct--) {
            putchar(map[rand()%map_ct]);
        }
        putchar(10);
    }
    return 0;
}
