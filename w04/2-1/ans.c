#include <stdio.h>
int input[1050][1050];
int ans[1050][1050];
int mv[8][2] = {
    {-1, -1},
    {-1, 0},
    {-1, 1},
    {0, -1},
    {0, 1},
    {1, -1},
    {1, 0},
    {1, 1}
};
int main(int argc, char *argv[])
{
    int n, m, i, j, k;
    scanf("%d%d", &n, &m);
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= m; j++) {
            scanf("%d", &input[i][j]);
        }
    }
    for (i = 1; i <= n; i++) {
        for (j = 1; j <= m; j++) {
            for (k = 0; k < 8; k++) {
                ans[i][j] += input[i + mv[k][0]][j + mv[k][1]];
            }
        }
    }
    for (i = 1; i <= n; i++) {
        printf("%d", ans[i][1]);
        for (j = 2; j <= m; j++) {
            printf(" %d", ans[i][j]);
        }
        printf("\n");
    }
    return 0;
}
